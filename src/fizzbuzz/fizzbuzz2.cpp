#include <iostream>
using namespace std;
int main(){
	for(int i=1; i<=100; i++){
		bool fizz = (i % 3) == 0;
		bool buzz = (i % 5) == 0;
		int fb = fizz + buzz;
		switch(fb){
			case 1:
				if(fizz){
					cout<<"Fizz";
				}
				else{
					cout<<"Buzz";
				}
				break;
			case 2:
				cout<<"FizzBuzz";
				break;
			default:
				cout<<i;
				break;
		}
		cout<<"\n";
	}
}