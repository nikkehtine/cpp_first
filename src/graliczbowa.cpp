#include <iostream>
#include <cstdlib>
#include <time.h>
#include <stdio.h>

using namespace std;

int liczba, strzal, proba=0;

int main()
{
	cout<<"Zagrajmy w grę... Odgadnij moją liczbę od 1 do 100!"<<endl;
	srand(time(NULL));
	liczba = rand()%100+1;

	while(strzal!=liczba)
	{
		proba++;
		cout<<"Zgadnij liczbę ("<<proba<<" próba): ";
		cin>>strzal;

		if(strzal==liczba)
			cout<<"Wygrałeś za "<<proba<<" podejściem!"<<endl;
		if(strzal<liczba)
			cout<<"Za mało"<<endl;
		if(strzal>liczba)
			cout<<"Za dużo"<<endl;
	}

	return 0;
}